package methods

import (
	"image/color"
	"math"
)

// Point denotes a coordinate in 2d space
type Point struct{ X, Y float64 }

// Path is a collection of points
type Path []Point

// Distance computes the distance between two points
func (p Point) Distance(q Point) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}

// Distance computes the entire distance of multiple points
func (path Path) Distance() float64 {
	sum := 0.0
	for i := range path {
		if i > 0 {
			sum += path[i-1].Distance(path[i])
		}
	}
	return sum
}

// ScaleBy scales the point
func (p *Point) ScaleBy(factor float64) {
	p.X *= factor
	p.Y *= factor
}

// IntList is a linked list of integers.
// A nil *IntList represents an empty list
type IntList struct {
	Value int
	Tail  *IntList
}

// Sum adds all the elements of the IntList
func (list *IntList) Sum() int {
	if list == nil {
		return 0
	}
	return list.Value + list.Tail.Sum()
}

// ColoredPoint is `Point` with a color
type ColoredPoint struct {
	Point
	color.RGBA
}

// ColoredPointPointer is a pointer to `Point` with a color
type ColoredPointPointer struct {
	*Point
	color.RGBA
}
