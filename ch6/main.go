package main

import (
	"fmt"
	"image/color"
	"sync"

	"bitbucket.com/vivek-26/learn-golang/ch6/methods"
)

var cache = struct {
	sync.Mutex
	mapping map[string]string
}{mapping: make(map[string]string)}

func main() {
	p := methods.Point{X: 1, Y: 2}
	q := methods.Point{X: 4, Y: 6}
	fmt.Println("Distance between p and q is ➡️  ", p.Distance(q))

	perim := methods.Path{{X: 1, Y: 1}, {X: 5, Y: 1}, {X: 5, Y: 4}, {X: 1, Y: 1}}
	fmt.Println("Perimeter of path is ➡️  ", perim.Distance())

	// Three ways to calling methods with pointer receiver
	r := &methods.Point{X: 1, Y: 2}
	r.ScaleBy(2)
	fmt.Println("Pointer receiver 1 ➡️  ", *r)

	m := methods.Point{X: 1, Y: 2}
	pptr := &m
	pptr.ScaleBy(2)
	fmt.Println("Pointer receiver 2 ➡️  ", m)

	n := methods.Point{X: 1, Y: 2}
	(&n).ScaleBy(2)
	fmt.Println("Pointer receiver 3 ➡️  ", n)

	list := methods.IntList{Value: 5, Tail: &methods.IntList{Value: 5, Tail: nil}}
	fmt.Println("IntList sum is ➡️  ", list.Sum())

	cPoint := methods.ColoredPoint{
		methods.Point{X: 1, Y: 2},
		color.RGBA{R: 0, G: 255, B: 0, A: 255},
	}
	cPoint.ScaleBy(2)
	fmt.Println("Composing Types by embedding ➡️  ", cPoint)

	cPointer := methods.ColoredPointPointer{
		&methods.Point{X: 1, Y: 2},
		color.RGBA{R: 0, G: 0, B: 255, A: 255},
	}
	cPointer.ScaleBy(2)
	fmt.Println("Composing Types by embedding ➡️  ", *cPointer.Point)

	cache.mapping["key"] = "value"
	fmt.Println("Cache Lookup Value ➡️  ", Lookup("key"))

	// Method Values
	distanceFromP := p.Distance
	fmt.Println("Method Values ➡️  ", distanceFromP(q))

	// Method Expression
	distance := methods.Point.Distance
	fmt.Println("Method Expression ➡️  ", distance(p, q))

	scale := (*methods.Point).ScaleBy
	scale(&p, 5)
	fmt.Println("Method Expression ➡️  ", p)
}

// Lookup searches the cache for a value
func Lookup(key string) string {
	cache.Lock()
	v := cache.mapping[key]
	cache.Unlock()
	return v
}
