package main

import (
	"log"

	"bitbucket.com/vivek-26/learn-golang/ch5/functions"
)

func main() {
	functions.Signature()

	slice, err := functions.LogLangSlice()
	log.Println("Logging Lang Slice ➡️ ", slice, err)

	log.Println("Bare Return in Go ➡️ ", functions.BareReturn())
	log.Println("Zero value of a function is nil ➡️ ", functions.ZeroValue())

	log.Println("String Map in Go ➡️ ", functions.StringMap())

	f := functions.Squares()
	log.Println("Square using Closure ➡️ ", f())
	log.Println("Square using Closure ➡️ ", f())
	log.Println("Square using Closure ➡️ ", f())

	log.Println("Fibonacci Sum ➡️ ", functions.ComputeFib(20))

	functions.CaptureIterationVar()

	log.Println("Variadic Sum ➡️ ", functions.Sum())
	log.Println("Variadic Sum ➡️ ", functions.Sum(1, 2, 3, 4, 5))
	values := []int{1, 2, 3, 4, 5}
	log.Println("Variadic Sum ➡️ ", functions.Sum(values...))

	functions.DeferTrace()
	log.Println("Defer modify result ➡️ ", functions.Triple(30))

	functions.RecoverError()
}
