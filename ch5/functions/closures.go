package functions

//Squares ▶️  Computing squares of numbers using Closures in Go
func Squares() func() int {
	var x int

	return func() int {
		x++
		return x * x
	}
}
