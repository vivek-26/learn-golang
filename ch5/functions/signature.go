package functions

import (
	"fmt"
)

func add(x int, y int) int { return x + y }
func sub(x, y int) (z int) { return x - y }
func first(x, _ int) int   { return x }
func zero(int, int) int    { return 0 }

//Signature ▶️  Print function signature in Go
func Signature() {
	fmt.Printf("add ➡️  %T\n", add)
	fmt.Printf("sub ➡️  %T\n", sub)
	fmt.Printf("first ➡️  %T\n", first)
	fmt.Printf("zero ➡️  %T\n", zero)
}
