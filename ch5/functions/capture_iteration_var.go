package functions

import (
	"fmt"
)

//CaptureIterationVar ▶️  Capturing Iteration Variable in Go
func CaptureIterationVar() {
	var functions []func()
	var str = []string{"Golang", "Rust"}

	for _, val := range str {
		s := val // This is necessary
		functions = append(functions, func() {
			fmt.Println("CaptureIterationVar ➡️ ", s)
		})
	}

	for _, f := range functions {
		f()
	}
}
