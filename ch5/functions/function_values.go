package functions

import (
	"strings"
)

var f func(int) int

//ZeroValue ▶️  Nil value of a function
func ZeroValue() bool {
	return f == nil
}

func addOne(r rune) rune { return r + 1 }

//StringMap ▶️  String Map in Go
func StringMap() string {
	return strings.Map(addOne, "Admix")
}
