package functions

import (
	"log"
	"time"
)

func trace(msg string) func() {
	start := time.Now()
	log.Printf("Func %s started at %v", msg, start)
	return func() { log.Printf("Func %s finished in %v", msg, time.Since(start)) }
}

//DeferTrace ▶️  Tracing function execution time using defer
func DeferTrace() {
	defer trace("DeferTrace")()

	time.Sleep(1 * time.Second)
}

//Triple ▶️  Multiply by 3
func Triple(num int) (res int) {
	defer func() { res += num; return }()
	res = num * 2
	return
}
