package functions

import (
	"fmt"
)

func langSlice(str []string) ([]string, error) {
	if str == nil {
		return nil, fmt.Errorf("Empty slice passed")
	}

	return str, nil
}

//LogLangSlice ▶️  Return type of a function
func LogLangSlice() ([]string, error) {
	return langSlice([]string{})
}

//BareReturn ▶️  Bare Returns in Go
func BareReturn() (z int) {
	z = 1
	return
}
