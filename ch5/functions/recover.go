package functions

import (
	"fmt"
)

func recoverFromError() {
	if p := recover(); p != nil {
		fmt.Printf("Recovered from error:  %v\n", p)
	}
}

//RecoverError ▶️  recover() use in Go
func RecoverError() {
	defer recoverFromError()
	panic("Stupid error...")
}
