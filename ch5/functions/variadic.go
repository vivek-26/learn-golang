package functions

//Sum ▶️  Variadic Sum Function
func Sum(vals ...int) int {
	total := 0
	for _, v := range vals {
		total += v
	}
	return total
}
