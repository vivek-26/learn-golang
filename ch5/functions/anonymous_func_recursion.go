package functions

//ComputeFib ▶️  Computing Fibonacci using anonymous function and recursion
func ComputeFib(x int) int {
	var fib func(z int) int

	fib = func(z int) int {
		if z == 0 || z == 1 {
			return z
		}

		return fib(z-1) + fib(z-2)
	}

	return fib(x)
}
