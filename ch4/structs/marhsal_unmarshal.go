package structs

import (
	"encoding/json"
	"fmt"
	"log"
)

//MovieTitle ▶️   Movie Title Struct
type MovieTitle struct{ Title string }

var data []byte

//JSONMarshal ▶️  Shows conversion of struct to json string
func JSONMarshal() {
	// List of movies
	movies := []Movie{
		{Title: "Casablanca", Year: 1942, Color: false, Actors: []string{"Humphrey Bogart", "Ingrid Bergman"}},
		{Title: "Cool Hand Luke", Year: 1967, Color: true, Actors: []string{"Paul Newman"}},
		{Title: "Bullit", Year: 1968, Color: true, Actors: []string{"Steve McQueen", "Jacqueline Bisset"}},
	}

	// Convert struct array to json
	data, _ = json.Marshal(movies)
	fmt.Printf("JSON Marshal ▶️   %s\n", data)
}

//JSONUnmarshal ▶️   Shows json string to struct conversion
func JSONUnmarshal() {
	var titles []MovieTitle

	if err := json.Unmarshal(data, &titles); err != nil {
		log.Fatalf("JSON Unmarshalling failed ▶️   %s\n", err)
	}

	fmt.Printf("Unmarshalled Struct ▶️   %v\n", titles)
}
