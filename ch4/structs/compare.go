package structs

import (
	"fmt"
)

//Point ▶️ Define a 2D point
type Point struct{ X, Y int }

//Compare ▶️ Comparing struct in Go
func Compare() {
	p1 := Point{5, 7}
	p2 := Point{X: 5, Y: 7}

	fmt.Printf("p1 == p2 ▶️  %t ✔️\n", p1 == p2)
}
