package structs

import (
	"fmt"
	"time"
)

//Movie struct
type Movie struct {
	Title  string
	Year   int  `json:"released"`
	Color  bool `json:"color,omitempty"`
	Actors []string
}

//Employee struct
type Employee struct {
	ID                      int
	Name, Address, Position string
	DOB                     time.Time
	Salary                  int
	ManagerID               int
}

//Init shows struct initialization
func Init() {
	var e Employee

	e.Salary = 70000 // Amazing...
	position := &e.Position
	*position = "Senior Software Developer"

	var emplOfMonth *Employee = &e
	emplOfMonth.ID = 12345
	(*emplOfMonth).ManagerID = 123
	fmt.Printf("Struct Initialization: %v\n", e)
}

//Literals shows struct literals usage in Go
func Literals() {
	emp1 := Employee{100, "Emp Name", "Address", "Senior Developer", time.Now(), 700000, 50}
	emp2 := Employee{ID: 100, Name: "Emp Name", Address: "Address", Position: "Senior Developer", DOB: time.Now(), Salary: 7000000, ManagerID: 50}
	fmt.Printf("Struct Literal Type 1 ➡️ %v\n", emp1)
	fmt.Printf("Struct Literal Type 2 ➡️ %v\n", emp2)

	Bonus(&emp2, 50)
	fmt.Printf("After getting bonus ➡️ %v 🚀 😄\n", emp2)
}

//Bonus ▶️ Give the employee a raise, make them happy!
func Bonus(e *Employee, percent int) {
	e.Salary = e.Salary * percent / 100
}
