package slice

import (
	"fmt"
	"strings"
)

func modify(slice []string) {
	for i := range slice {
		slice[i] = strings.ToLower(slice[i])
	}
	fmt.Printf("After for loop: %v\n", slice)
}

//Copy calls modify and passes a slice as argument. It is passed by reference.
func Copy() {
	slice := Months[10:13]
	fmt.Printf("Before calling modify: %v\n", slice)
	modify(slice)
	fmt.Printf("After calling modify: %v\n", slice)
	fmt.Printf("Months slice after calling modify: %v\n", Months)
}
