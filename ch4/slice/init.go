package slice

import (
	"fmt"
)

//Months Slice holding all months in a year
var Months = [...]string{1: "Jan", 2: "Feb", 3: "Mar",
	4: "Apr", 5: "May", 6: "Jun",
	7: "Jul", 8: "Aug", 9: "Sep",
	10: "Oct", 11: "Nov", 12: "Dec"}

//Init Slice initialization
func Init() {
	fmt.Printf("Months Slice: %v\n", Months)

	// Slice using `make`, Length: 5, Capacity: 10
	es := make([]int8, 5, 10)
	fmt.Printf("Empty Int8 Slice: %v, Len: %d, Cap: %d\n", es, len(es), cap(es))

	// Length = Capacity = 5
	aes := make([]int8, 5)
	fmt.Printf("Another Empty Int8 Slice: %v, Len: %d, Cap: %d\n", aes, len(aes), cap(aes))
}
