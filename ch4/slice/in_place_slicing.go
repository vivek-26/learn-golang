package slice

import (
	"fmt"
)

//InPlaceSlicing shows the same in Go.
func InPlaceSlicing() {
	strings := []string{"One", "", "Three"}
	fmt.Println("In Place Slicing", nonEmpty(strings))
}

func nonEmpty(str []string) []string {
	result := str[:0] // Zero length slice of original

	for i := range str {
		if str[i] != "" {
			result = append(result, str[i])
		}
	}

	return result
}
