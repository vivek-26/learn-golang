package slice

import (
	"fmt"
)

//Append Appending elements in slice
func Append() {
	slice := make([]int8, 3, 10)

	for i := 0; i < 11; i++ {
		slice = append(slice, 1)
		fmt.Printf("Position: %d, Len: %d, Cap: %d, Slice: %v\n", i+1, len(slice), cap(slice), slice)
	}

	slice = append(slice, 2, 3)
	fmt.Printf("Len: %d, Cap: %d, Slice: %v\n", len(slice), cap(slice), slice)
	anotherSlice := []int8{4, 5, 6}
	slice = append(slice, anotherSlice...)
	fmt.Printf("Len: %d, Cap: %d, Slice: %v\n", len(slice), cap(slice), slice)
}
