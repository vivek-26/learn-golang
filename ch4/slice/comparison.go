package slice

import (
	"fmt"
)

func checkEquals(x, y []string) bool {
	if len(x) != len(y) {
		return false
	}

	for i := range x {
		if x[i] != y[i] {
			return false
		}
	}

	return true
}

//Compare Slice elements. Slice elements are not compared as in the
// case of arrays.
func Compare() {
	slice1 := Months[1:4]
	slice2 := Months[1:4]
	fmt.Println(checkEquals(slice1, slice2))
}
