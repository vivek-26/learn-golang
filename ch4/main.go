package main

import (
	"bitbucket.com/vivek-26/learn-golang/ch4/array"
	"bitbucket.com/vivek-26/learn-golang/ch4/maps"
	"bitbucket.com/vivek-26/learn-golang/ch4/slice"
	"bitbucket.com/vivek-26/learn-golang/ch4/structs"
)

func main() {
	array.Init()
	array.Compare()
	array.Copy()

	slice.Init()
	slice.Compare()
	slice.Copy()
	slice.Append()
	slice.InPlaceSlicing()

	maps.Init()
	maps.Range()
	maps.Sort()
	maps.Compare()
	maps.CustomKeyInMap()

	structs.Init()
	structs.Literals()
	structs.Compare()
	structs.JSONMarshal()
	structs.JSONUnmarshal()
}
