package maps

import (
	"fmt"
)

//Compare two maps
func Compare() {
	userL1 := map[string]int{"A": 1, "B": 2, "C": 3}
	userL2 := map[string]int{"C": 3, "A": 1, "B": 2}

	if len(userL1) != len(userL2) {
		fmt.Println("Maps are not equal (Length)")
		return
	}

	for name, id := range userL1 {
		if v, ok := userL2[name]; !ok || v != id {
			fmt.Println("Maps are not equal (Key/Values)")
			return
		}
	}

	fmt.Println("Maps are equal")
}
