package maps

import (
	"fmt"
)

var m = make(map[string]int)

//CustomKeyInMap shows the usage of non comparable types (eg - slices)
// as keys in maps
func CustomKeyInMap() {
	key := []string{0: "Hello", 1: "Golang"}
	add(key)
	add(key)
	add(key)
	fmt.Printf("Count for key: %v is %d\n", key, count(key))
}

func key(list []string) string { return fmt.Sprintf("%q", list) }

func add(list []string) { m[key(list)]++ }

func count(list []string) int { return m[key(list)] }
