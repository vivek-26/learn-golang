package maps

import (
	"fmt"
	"sort"
)

var users = map[string]int{"B": 1, "A": 2, "C": 3}

//Range over all map elements
func Range() {
	for name, id := range users {
		fmt.Printf("Username -> %v, ID -> %v\n", name, id)
	}
}

//Sort the keys of a map
func Sort() {
	var names []string
	for name := range users {
		names = append(names, name)
	}

	sort.Strings(names)

	for _, name := range names {
		fmt.Printf("Username -> %v, ID -> %v\n", name, users[name])
	}
}
