package maps

import (
	"fmt"
)

//Init Map initialization
func Init() {
	mapOne := make(map[string]int) // Creates an empty map
	mapTwo := map[string]int{
		"Alice":   41,
		"Charlie": 49,
	}

	fmt.Printf("Map One: %v\n", mapOne)
	fmt.Printf("Map Two: %v\n", mapTwo)

	// Map assignment
	mapTwo["Alice"] = 45
	fmt.Printf("Map Two After Assignment: %v\n", mapTwo)

	// Map delete
	delete(mapTwo, "Alice")
	fmt.Printf("Map Two After Deletion: %v\n", mapTwo)

	// Zero value is returned if we try to retrieve a key which is not present
	mapTwo["Bob"] = mapTwo["Bob"] + 1
	fmt.Printf("Map Two After using Zero Value: %v\n", mapTwo)

	// Zero value of a map type
	var users map[int]string
	fmt.Println("Zero value of map is nil", users == nil)
	fmt.Println("Length of nil map is zero", len(users) == 0)
	// users["A"] = 1 --> panic, cannot assign to nil map.
	// You must allocate the map before you store in it.
}
