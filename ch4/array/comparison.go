package array

import (
	"fmt"
)

//Compare Array comparison
func Compare() {
	a := [2]int{1, 2}
	b := [...]int{1, 2}
	c := [2]int{1, 3}
	d := [3]int{1, 2}
	fmt.Println(a == b, b == c, a == c, d)
}
