package array

import (
	"fmt"
)

type currency int

const (
	usd currency = iota
	eur
	gbp
	rmb
)

//Init --> initializing arrays
func Init() {
	var a [3]int

	fmt.Println("First value --> ", a[0])

	// Iterating over arrays
	for i, v := range a {
		fmt.Printf("(i: %d, v: %d)\n", i, v)
	}

	// Array literal syntax
	var b = [3]int{1, 2, 3}
	fmt.Printf("Array Literal Syntax: %v\n", b)

	var c = [...]int{1, 2, 3}
	fmt.Printf("Array Literal Syntax: %T %v\n", c, c)

	// In array literal syntax, indexes can also be specified.
	symbol := [...]string{usd: "$", eur: "€", gbp: "£", rmb: "¥"}
	fmt.Println("European Symbol:", symbol[eur])

	r := [...]int{9: 1}
	fmt.Printf("Array literal syntax without all elements: %v\n", r)
}
