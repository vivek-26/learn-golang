package array

import (
	"fmt"
)

func modify(a [3]int) {
	for i := range a {
		a[i] = 1
	}

	fmt.Printf("Array after for loop: %v\n", a)
}

func modifyUsingRef(ptr *[3]int) {
	for i := range ptr {
		ptr[i] = 1
	}

	fmt.Printf("Array after for loop: %v\n", ptr)
}

//Copy shows that arrays are copied. Their reference is not passed.
func Copy() {
	var a [3]int
	fmt.Printf("Before calling modify: %v\n", a)
	modify(a)
	fmt.Printf("After calling modify: %v\n", a)

	var b [3]int
	fmt.Printf("Before calling modify: %v\n", b)
	modifyUsingRef(&b)
	fmt.Printf("After calling modify: %v\n", b)
}
