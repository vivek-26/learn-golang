package constants

type weekday int

//Using `iota` for Weekdays initialization
const (
	Sunday weekday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)

//Computing size using `iota` expression
const (
	_ = 1 << (10 * iota)
	KiB
	MiB
	GiB
	TiB
)
