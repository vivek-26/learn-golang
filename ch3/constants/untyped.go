package constants

import (
	"fmt"
)

//UntypedConstants Untyped Constants types in Go
func UntypedConstants() {
	fmt.Printf("%T\n", 0)
	fmt.Printf("%T\n", 0.0)
	fmt.Printf("%T\n", 0i)
	fmt.Printf("%T\n", '\000') // int32 (rune)
}
