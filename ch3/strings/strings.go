package strings

import (
	"fmt"
)

//StrLen Returns length of string
func StrLen() {
	s := "Hello World"
	fmt.Printf("Length of String is %d\n", len(s))
}

//StrSlice String Slicing
func StrSlice() {
	s := "Hello World"
	fmt.Printf("s[0:5] %s\n", s[0:5])

	fmt.Printf("s[:5] %s\n", s[:5])
	fmt.Println(s[:5] + " Golang")
}
