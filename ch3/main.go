package main

import (
	"fmt"

	"bitbucket.com/vivek-26/learn-golang/ch3/constants"
	"bitbucket.com/vivek-26/learn-golang/ch3/strings"
)

func main() {
	strings.StrLen()
	strings.StrSlice()
	fmt.Println("Constants Group", constants.A, constants.B, constants.C, constants.D)
	fmt.Println("Weekdays Using `iota`", constants.Sunday, constants.Monday)
	fmt.Println("Sizes Using `iota`", constants.KiB, constants.MiB, constants.GiB)
	constants.UntypedConstants()
}
