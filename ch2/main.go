package main

import (
	"fmt"

	"bitbucket.com/vivek-26/learn-golang/ch2/pointers"
	"bitbucket.com/vivek-26/learn-golang/ch2/tempconv"
)

func main() {
	fmt.Println(pointers.F() == pointers.F())
	fmt.Println(tempconv.CToF(21))
	fmt.Println(tempconv.FToC(69.8))
}
