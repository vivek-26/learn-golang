package pointers

//F Weird pointer property in Golang
func F() *int {
	v := 1
	return &v
}
